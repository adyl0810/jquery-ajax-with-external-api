$(async () => {

    $(document).ajaxStart(function () {
        $('#preloader').preloader({
            text : 'Loading'
        })
    })

    $(document).ajaxStop(function () {
        $('#preloader').preloader('remove')
    })


    $("#character-search-btn").on('click', function () {
        let characterId = $("#number-character").val()
        $.get(`https://rickandmortyapi.com/api/character/${characterId}`, function (data) {
            $("#character-image").attr("src", data.image)
            $("#character-id").text("Id: " + data.id)
            $("#character-name").text(data.name)
            $("#character-status").text("Status: " + data.status)
            $("#character-species").text("Species: " + data.species)
            $("#character-gender").text("Gender: " + data.gender)
            $("#character-origin").text("Origin: " + data.origin.name)
            $("#character-location").text("Current location: " + data.location.name)
        }).fail(function (jqXHR) {
            if(jqXHR.status === 404) {
                alert("not found")
            }




        })
        $("#character-card").removeAttr('hidden')
    })

    $("#episode-search-btn").on('click', function () {
        let episodeId = $("#number-episode").val()
        $("#episode-characters").empty()
        $.get(`https://rickandmortyapi.com/api/episode/${episodeId}`, function (data) {
            $("#episode-id").text("Id: " + data.id)
            $("#episode-name").text(data.name)
            $("#episode-air-date").text("Aired: " + data.air_date)
            for (let i = 0; i < data.characters.length; i++) {
                $.get(data.characters[i], function (characterData) {
                    $("#episode-characters").append(characterData.name + `<br>`)
                })
            }
        }).fail(function (jqXHR) {
            if(jqXHR.status === 404) {
                alert("not found")
            }
        })
        $("#episode-card").removeAttr('hidden')
    })

})